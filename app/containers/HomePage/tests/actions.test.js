import {
  CHANGE_USERNAME,
  ADD_ITEM,
  DELETE_TODO,
  CHECK_TODO,
  FILTER_TODO,
  EDIT_TODO,
  TOOGLE_ITEM,
  CLEAR_COMPLETED,
  CALL_API,
  RESPON,
} from '../constants';

import {
  changeUsername,
  addItem,
  deleteTodo,
  checkTodo,
  filterTodo,
  editTodo,
  toogleItem,
  clearCompleted,
  callAPI,
  respon,
} from '../actions';

describe('Home Actions', () => {
  describe('changeUsername', () => {
    it('should return the correct type and the passed name', () => {
      const fixture = 'Max';
      const expectedResult = {
        type: CHANGE_USERNAME,
        username: fixture,
      };

      expect(changeUsername(fixture)).toEqual(expectedResult);
    });
  });
});
describe('addItem', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = 'hihi';
    const expectedResult = {
      type: ADD_ITEM,
      name: fixture,
    };

    expect(addItem(fixture)).toEqual(expectedResult);
  });
});
describe('deleteTodo', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = 1;
    const expectedResult = {
      type: DELETE_TODO,
      itemdel: fixture,
    };

    expect(deleteTodo(fixture)).toEqual(expectedResult);
  });
});
describe('checkTodo', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = '12';
    const indexfix = 1;
    const expectedResult = {
      type: CHECK_TODO,
      itemCheck: fixture,
      index: indexfix,
    };

    expect(checkTodo(fixture, indexfix)).toEqual(expectedResult);
  });
});

describe('filterTodo', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = 'name123';
    const expectedResult = {
      type: FILTER_TODO,
      name: fixture,
    };

    expect(filterTodo(fixture)).toEqual(expectedResult);
  });
});

describe('editTodo', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = 'hihi';
    const id = 0;
    const expectedResult = {
      type: EDIT_TODO,
      id,
      name: fixture,
    };

    expect(editTodo(id, fixture)).toEqual(expectedResult);
  });
});

describe('toogleItem', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = true;
    const id = 1;
    const expectedResult = {
      type: TOOGLE_ITEM,
      checkedItem: fixture,
      id,
    };

    expect(toogleItem(fixture, id)).toEqual(expectedResult);
  });
});

describe('clearCompleted', () => {
  it('should return the correct type and the passed name', () => {
    const expectedResult = {
      type: CLEAR_COMPLETED,
    };

    expect(clearCompleted()).toEqual(expectedResult);
  });
});

describe('callAPI', () => {
  it('should return the correct type and the passed name', () => {
    const expectedResult = {
      type: CALL_API,
    };

    expect(callAPI()).toEqual(expectedResult);
  });
});

describe('respon', () => {
  it('should return the correct type and the passed name', () => {
    const fixture = 'http://123';
    const expectedResult = {
      type: RESPON,
      image: fixture,
    };

    expect(respon(fixture)).toEqual(expectedResult);
  });
});
