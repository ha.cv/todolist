/**
 * Tests for HomePage sagas
 */

import { put, takeLatest } from 'redux-saga/effects';

import { LOAD_REPOS } from 'containers/App/constants';
import { CALL_API } from 'containers/HomePage/constants';

import { reposLoaded, repoLoadingError } from 'containers/App/actions';
import { respon } from 'containers/HomePage/actions';

import githubData, { getRepos, getImageAPI } from '../saga';

const username = 'mxstbr';

/* eslint-disable redux-saga/yield-effects */
describe('Saga', () => {
  let getReposGenerator;
  let getImageGenerator;
  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    getReposGenerator = getRepos();
    getImageGenerator = getImageAPI();

    const selectDescriptor = getReposGenerator.next().value;
    expect(selectDescriptor).toMatchSnapshot();
    const callDescriptor = getReposGenerator.next(username).value;
    expect(callDescriptor).toMatchSnapshot();

    const callImageAPI = getImageGenerator.next().value;
    expect(callImageAPI).toMatchSnapshot();
  });

  it('should dispatch the reposLoaded, respon action if it requests the data successfully', () => {
    const response = [
      {
        name: 'First repo',
      },
      {
        name: 'Second repo',
      },
    ];
    const image = {
      message: 'https://images.dog.ceo/breeds/african/n02116738_10024.jpg',
      status: 'success',
    };
    const putDescriptor = getReposGenerator.next(response).value;
    expect(putDescriptor).toEqual(put(reposLoaded(response, username)));

    const putImageAPI = getImageGenerator.next(image).value;
    expect(putImageAPI).toEqual(put(respon(image.message)));
  });

  it('should call the repoLoadingError action if the response errors', () => {
    const response = new Error('Some error');
    const putDescriptor = getReposGenerator.throw(response).value;
    expect(putDescriptor).toEqual(put(repoLoadingError(response)));
  });

  it('should call the repoLoadingError action if the response errors', () => {
    const response = new Error('Some error');
    const putDescriptor = getImageGenerator.throw(response).value;
    expect(putDescriptor).toEqual(put(repoLoadingError(response)));
  });
});
describe('githubDataSaga Saga', () => {
  const githubDataSaga = githubData();

  it('should start task to watch for action', () => {
    const takeLatestDescriptor = githubDataSaga.next().value;
    expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_REPOS, getRepos));
    expect(takeLatestDescriptor).toEqual(takeLatest(CALL_API, getRepos));
  });
});
