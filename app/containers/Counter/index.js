import React, { useState } from 'react';
function Counter() {
  const [count, setCount] = useState(0);

  const onSetCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="main-counter">
      <button type="button" onClick={onSetCount}>
        +
      </button>
      <p data-testid="count">{count}</p>
    </div>
  );
}

export default Counter;
