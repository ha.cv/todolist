import produce from 'immer';

import homeReducer from '../reducer';
import {
  changeUsername,
  addItem,
  deleteTodo,
  checkTodo,
  filterTodo,
  editTodo,
  toogleItem,
  clearCompleted,
  respon,
} from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('homeReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      username: '',
      list: {},
      name: '',
      image: '',
    };
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(homeReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the changeUsername action correctly', () => {
    const fixture = 'mxstbr';
    const expectedResult = produce(state, draft => {
      draft.username = fixture;
    });

    expect(homeReducer(state, changeUsername(fixture))).toEqual(expectedResult);
  });

  it('should handle the addItem action correctly', () => {
    const fixture = 'hihi';
    const expectedResult = produce(state, draft => {
      draft.list[0] = {
        id: 0,
        name: 'hihi',
        isCompleted: false,
      };
    });

    expect(homeReducer(state, addItem(fixture))).toEqual(expectedResult);
  });

  it('should handle the deleteTodo action correctly', () => {
    const id = 0;
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted: false,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };
    const expectedResult = produce(state, draft => {
      delete draft.list[0];
    });

    expect(homeReducer(state, deleteTodo(id))).toEqual(expectedResult);
  });

  it('should handle the filterTodo action correctly', () => {
    const fixture = 'ACTIVE_TODO';
    const expectedResult = produce(state, draft => {
      draft.name = fixture;
    });

    expect(homeReducer(state, filterTodo(fixture))).toEqual(expectedResult);
  });

  it('should handle the editTodo action correctly', () => {
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted: false,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };
    const nametest = 'hihihi';
    const expectedResult = produce(state, draft => {
      draft.list[0].name = nametest;
    });

    expect(homeReducer(state, editTodo(0, nametest))).toEqual(expectedResult);
  });

  it('should handle the toogleItem action correctly', () => {
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted: false,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };
    const checkedItem = false;
    const expectedResult = produce(state, draft => {
      if (checkedItem) {
        Object.values(fixture).forEach(item => {
          draft.list[item.id].isCompleted = true;
        });
      }
    });

    expect(homeReducer(state, toogleItem(checkedItem))).toEqual(expectedResult);
  });

  it('should handle the toogleItem action correctly', () => {
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted: true,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };
    const checkedItem = true;
    const expectedResult = produce(state, draft => {
      if (!checkedItem) {
        Object.values(fixture).forEach(item => {
          draft.list[item.id].isCompleted = false;
        });
      }
    });

    expect(homeReducer(state, toogleItem(checkedItem))).toEqual(expectedResult);
  });

  it('should handle the clearCompleted action correctly', () => {
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted: false,
      },
      1: {
        id: 1,
        name: 'hihi',
        isCompleted: true,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };
    const expectedResult = produce(state, draft => {
      const arrTodo = Object.values(draft.list);
      const arrCompleted = arrTodo.filter(item => item.isCompleted);
      arrCompleted.forEach(item => {
        delete draft.list[item.id];
      });
    });
    expect(homeReducer(state, clearCompleted())).toEqual(expectedResult);
  });

  it('should handle the respon action correctly', () => {
    const fixture = 'http://123';
    const expectedResult = produce(state, draft => {
      draft.image = fixture;
    });

    expect(homeReducer(state, respon(fixture))).toEqual(expectedResult);
  });

  it('should handle the checkTodo action correctly', () => {
    const isCompleted = true;
    const id = 0;
    const fixture = {
      0: {
        id: 0,
        name: 'hihi',
        isCompleted,
      },
      1: {
        id: 1,
        name: '123',
        isCompleted: false,
      },
    };
    state = {
      username: '',
      list: fixture,
      name: '',
      image: '',
    };

    const expectedResult = produce(state, draft => {
      draft.list[0].isCompleted = true;
    });

    expect(homeReducer(state, checkTodo(isCompleted, id))).toEqual(
      expectedResult,
    );
  });
});
