/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  // CHANGE_COUNT,
  ADD_ITEM,
  DELETE_TODO,
  CHECK_TODO,
  FILTER_TODO,
  EDIT_TODO,
  TOOGLE_ITEM,
  CLEAR_COMPLETED,
  CALL_API,
  RESPON,
} from './constants';
/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

// export function changeCount(count) {
//   return {
//     type: CHANGE_COUNT,
//     count,
//   };
// }

export function addItem(name) {
  return {
    type: ADD_ITEM,
    name,
  };
}

export function deleteTodo(itemdel) {
  return {
    type: DELETE_TODO,
    itemdel,
  };
}

export function checkTodo(itemCheck, index) {
  return {
    type: CHECK_TODO,
    itemCheck,
    index,
  };
}

export function filterTodo(name) {
  return {
    type: FILTER_TODO,
    name,
  };
}

export function editTodo(id, name) {
  return {
    type: EDIT_TODO,
    id,
    name,
  };
}

export function toogleItem(checkedItem, id) {
  return {
    type: TOOGLE_ITEM,
    checkedItem,
    id,
  };
}

export function clearCompleted() {
  return {
    type: CLEAR_COMPLETED,
  };
}

export function callAPI() {
  return {
    type: CALL_API,
  };
}

export function respon(image) {
  return {
    type: RESPON,
    image,
  };
}
