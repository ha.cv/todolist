import React from 'react';
import style from './style.css';
const operation = {
  '/': (prevValue, nextValue) => prevValue / nextValue,
  '*': (prevValue, nextValue) => prevValue * nextValue,
  '+': (prevValue, nextValue) => prevValue + nextValue,
  '-': (prevValue, nextValue) => prevValue - nextValue,
  '=': prevValue => prevValue,
};

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNumber: '',
      secondNumber: '',
      operator: '',
    };
  }

  calculate = e => {
    const currentValue = e.target.name;
    this.setState(state => ({
      secondNumber: state.secondNumber + currentValue,
    }));
  };

  remove = () => {
    this.setState(state => ({
      secondNumber: state.secondNumber.slice(0, -1),
    }));
  };

  reset = () => {
    this.setState({
      firstNumber: '',
      secondNumber: '',
      operator: '',
    });
  };

  nextOperation = nextoperator => {
    const { secondNumber, firstNumber, operator } = this.state;
    const currentvalue = Number(secondNumber);

    this.setState({
      firstNumber: currentvalue,
      secondNumber: '',
    });

    if (operator) {
      const newValue = operation[operator](Number(firstNumber), currentvalue);
      this.setState({
        firstNumber: newValue,
      });
    }

    this.setState({
      operator: nextoperator.target.name,
    });
  };

  render() {
    return (
      <div className={style}>
        <p className="calculator">{this.state.firstNumber}</p>
        <p className="output-display">{this.state.secondNumber}</p>
        <button
          className="number"
          name="1"
          type="button"
          onClick={this.calculate}
        >
          1
        </button>
        <button
          className="number"
          name="2"
          type="button"
          onClick={this.calculate}
        >
          2
        </button>
        <button
          className="number"
          name="3"
          type="button"
          onClick={this.calculate}
        >
          3
        </button>
        <button
          className="number"
          name="4"
          type="button"
          onClick={this.calculate}
        >
          4
        </button>{' '}
        <br />
        <button
          className="number"
          name="5"
          type="button"
          onClick={this.calculate}
        >
          5
        </button>
        <button
          className="number"
          name="6"
          type="button"
          onClick={this.calculate}
        >
          6
        </button>
        <button
          className="number"
          name="7"
          type="button"
          onClick={this.calculate}
        >
          7
        </button>
        <button
          className="number"
          name="8"
          type="button"
          onClick={this.calculate}
        >
          8
        </button>{' '}
        <br />
        <button
          className="number"
          name="9"
          type="button"
          onClick={this.calculate}
        >
          9
        </button>
        <button
          className="number-ope"
          name="+"
          type="button"
          onClick={this.nextOperation}
        >
          +
        </button>
        <button
          className="number-ope"
          name="-"
          type="button"
          onClick={this.nextOperation}
        >
          -
        </button>
        <button
          className="number-ope"
          name="*"
          type="button"
          onClick={this.nextOperation}
        >
          *
        </button>{' '}
        <br />
        <button
          className="number-ope equal"
          name="="
          type="button"
          onClick={this.nextOperation}
        >
          =
        </button>
        <button
          className="number-ope equal"
          name="/"
          type="button"
          onClick={this.nextOperation}
        >
          /
        </button>
        <button
          className="number-reset"
          name="C"
          type="button"
          onClick={this.reset}
        >
          C
        </button>
        <button
          className="number-remove"
          id="remove"
          name="CE"
          type="button"
          onClick={this.remove}
        >
          CE
        </button>
      </div>
    );
  }
}
export default Calculator;
