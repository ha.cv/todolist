import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import * as appActions from 'containers/App/actions';
import { browserHistory } from 'react-router-dom';
import configureStore from '../../../configureStore';
import Todolist from '../index';
const renderTodolist = store =>
  render(
    <Provider store={store}>
      <Todolist />
    </Provider>,
  );

describe('<Todolist />', () => {
  let store;
  beforeAll(() => {
    appActions.loadRepos = jest.fn(() => ({ type: '' }));
  });

  beforeEach(() => {
    store = configureStore({}, browserHistory);
    appActions.loadRepos.mockClear();
  });
  it('clearCompleted', () => {
    const { container, getByText } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const contain = container.querySelector('.contain');
    const btnClear = getByText('Clear completed');
    fireEvent.click(btnClear);
    expect(contain.childElementCount).toEqual(3);
  });
  it('active', () => {
    const { container, getByText } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const contain = container.querySelector('.contain');
    const btnActive = getByText('Active');
    fireEvent.click(btnActive);
    expect(contain.childElementCount).toEqual(3);
  });

  it('Completed', () => {
    const { container, getByText } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const contain = container.querySelector('.contain');
    const btnCompleted = getByText('Completed');
    fireEvent.click(btnCompleted);
    expect(contain.childElementCount).toEqual(2);
  });

  it('add', () => {
    const { container } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const contain = container.querySelector('.contain');
    expect(contain.childElementCount).toEqual(3);
  });

  it('delete', () => {
    const { container } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const btnDelete = container.querySelector('.button-delete');
    fireEvent.click(btnDelete);
    const contain = container.querySelector('.contain');
    expect(contain.childElementCount).toEqual(2);
  });

  it('toogle item', () => {
    const { container, getByText } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    const contain = container.querySelector('.contain');
    const btnCheckAll = container.querySelector('.label-checkall');
    const completed = getByText('Completed');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    fireEvent.click(btnCheckAll);
    fireEvent.click(completed);
    expect(contain.childElementCount).toEqual(3);
  });
  it('checked', () => {
    const { container } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const checkbox = container.querySelector('.item label');
    fireEvent.click(checkbox);
  });
  it('edit', () => {
    const { container } = renderTodolist(store);
    const form = container.querySelector('.form-add');
    const input = container.querySelector('.input-add');
    fireEvent.change(input, { target: { value: 'hihi' } });
    fireEvent.submit(form);
    const label = container.querySelector('.item label span');
    fireEvent.dblClick(label);
    const inputEdit = container.querySelector('.input-edit');
    fireEvent.keyDown(inputEdit, { key: 'Enter', code: 13 });
    fireEvent.keyDown(inputEdit, { key: 'A', code: 65 });
    fireEvent.blur(inputEdit);
  });
});
