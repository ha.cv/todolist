/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );
// const makeSelectCount = () =>
//   createSelector(
//     selectHome,
//     homeState => homeState.count,
//   );

const makeSelectList = () =>
  createSelector(
    selectHome,
    homeState => homeState.list,
  );

const nameSelector = () =>
  createSelector(
    selectHome,
    homeState => homeState.name,
  );
const responSelector = () =>
  createSelector(
    selectHome,
    globalState => globalState.image,
  );

export {
  selectHome,
  makeSelectUsername,
  // makeSelectCount,
  makeSelectList,
  nameSelector,
  responSelector,
};
