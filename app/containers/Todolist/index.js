import React from 'react';
import { createStructuredSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import {
  addItem,
  deleteTodo,
  checkTodo,
  filterTodo,
  editTodo,
  toogleItem,
  clearCompleted,
} from 'containers/HomePage/actions';
import {
  makeSelectList,
  nameSelector,
  responSelector,
} from 'containers/HomePage/selectors';
import reducer from 'containers/HomePage/reducer';
import saga from 'containers/HomePage/saga';
import style from './style.css';

const key = 'home';

const stateSelector = createStructuredSelector({
  repos: makeSelectRepos(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  list: makeSelectList(),
  name: nameSelector(),
  image: responSelector(),
});

export default function Todolist() {
  const { list, name } = useSelector(stateSelector);

  const input = React.createRef();
  const dispatch = useDispatch();
  const onDeleteTodo = e => {
    dispatch(deleteTodo(e.target.dataset.id));
  };
  const onCheckTodo = e => {
    dispatch(checkTodo(e.target.checked, e.target.dataset.id));
  };

  const onEdit = e => {
    e.target.parentNode.nextElementSibling.style.display = 'inline-block';
    e.target.parentNode.style.display = 'none';
    e.target.parentNode.nextElementSibling.nextElementSibling.style.display =
      'inline-block';
    e.target.parentNode.nextElementSibling.value = e.target.innerHTML;
  };
  const onSubmitValue = e => {
    if (e.key === 'Enter') {
      dispatch(editTodo(e.target.dataset.id, e.target.value));
      e.target.style.display = 'none';
      e.target.previousElementSibling.style.display = 'inline-block';
    }
  };

  const onBlurValue = e => {
    dispatch(editTodo(e.target.dataset.id, e.target.value));
    e.target.style.display = 'none';
    e.target.previousElementSibling.style.display = 'inline-block';
  };

  const arrTodo = Object.values(list);
  let visibleTodo = [];
  switch (name) {
    case 'ACTIVE_TODO':
      visibleTodo = arrTodo.filter(item => !item.isCompleted);
      break;
    case 'COMPLETED_TODO':
      visibleTodo = arrTodo.filter(item => item.isCompleted);
      break;
    default:
      visibleTodo = arrTodo;
      break;
  }

  const todo = visibleTodo.map((item, index) => (
    <div className="item" key={item.id}>
      <input
        id={item.id}
        className="styled-checkbox"
        type="checkbox"
        data-id={item.id}
        checked={visibleTodo[index].isCompleted}
        onChange={onCheckTodo}
      />
      <label htmlFor={item.id}>
        <span onDoubleClick={onEdit}>{item.name}</span>
      </label>
      <input
        data-id={item.id}
        className="input-edit"
        type="text"
        onKeyDown={onSubmitValue}
        onBlur={onBlurValue}
      />

      <button
        className="button-delete"
        data-id={item.id}
        type="button"
        onClick={onDeleteTodo}
      >
        x
      </button>
    </div>
  ));
  const onChangeList = e => {
    e.preventDefault();
    dispatch(addItem(input.current.value));
    input.current.value = '';
  };

  let countItem = 0;
  arrTodo.forEach(item => {
    if (!item.isCompleted) {
      countItem += 1;
    }
  });
  // arrTodo.filter(item => {
  //   if (!item.isCompleted) {
  //     countItem += 1;
  //   }
  // });
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const onToogleItem = e => {
    dispatch(toogleItem(e.target.checked));
  };

  const onClearCompleted = () => {
    dispatch(clearCompleted());
  };
  const filterItem = e => {
    const actionElement = document.querySelectorAll('.action button');
    actionElement.forEach(ele => {
      ele.setAttribute('class', 'action');
    });
    e.target.setAttribute('class', 'active');
    dispatch(filterTodo(e.target.dataset.name));
  };

  return (
    <div className="main-todolist">
      <form className="form-add" onSubmit={onChangeList}>
        <input className="input-add" id="text-input" type="text" ref={input} />
      </form>

      <div id="contain" className="contain">
        <input
          className="styled-checkbox"
          id="checkbox-all"
          type="checkbox"
          onChange={onToogleItem}
        />
        <label className="label-checkall" htmlFor="checkbox-all">
          {' '}
        </label>
        {todo}
      </div>
      <div className="action">
        <span className="item-left">{countItem} item left</span>
        <button
          className={style}
          type="button"
          data-name="ALL_TODO"
          onClick={filterItem}
        >
          All
        </button>
        <button type="button" data-name="ACTIVE_TODO" onClick={filterItem}>
          Active
        </button>
        <button type="button" data-name="COMPLETED_TODO" onClick={filterItem}>
          Completed
        </button>
        <button
          type="button"
          onClick={onClearCompleted}
          className="clear-completed"
        >
          Clear completed
        </button>
      </div>
    </div>
  );
}
