/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  CHANGE_USERNAME,
  // CHANGE_COUNT,
  ADD_ITEM,
  DELETE_TODO,
  CHECK_TODO,
  FILTER_TODO,
  EDIT_TODO,
  TOOGLE_ITEM,
  CLEAR_COMPLETED,
  RESPON,
} from './constants';

// The initial state of the App
export const initialState = {
  username: '',
  // count: 0,
  list: {},
  name: '',
  image: '',
};

let idItem = -1;

/* eslint-disable default-case, no-param-reassign */
const homeReducer = produce((draft, action) => {
  const arrList = Object.values(draft.list);
  switch (action.type) {
    case CHANGE_USERNAME:
      // Delete prefixed '@' from the github username
      draft.username = action.username.replace(/@/gi, '');
      break;
    // case CHANGE_COUNT:
    //   draft.count += 1;
    //   break;
    case ADD_ITEM:
      {
        const item = {
          id: (idItem += 1),
          name: action.name,
          isCompleted: false,
        };
        draft.list[idItem] = item;
      }
      break;
    case DELETE_TODO:
      delete draft.list[action.itemdel];
      break;
    case CHECK_TODO:
      draft.list[action.index].isCompleted = action.itemCheck;
      break;
    case FILTER_TODO:
      draft.name = action.name;
      break;
    case EDIT_TODO:
      draft.list[action.id].name = action.name;
      break;
    case TOOGLE_ITEM:
      if (action.checkedItem) {
        arrList.forEach(item => {
          draft.list[item.id].isCompleted = true;
        });
      } else {
        arrList.forEach(item => {
          draft.list[item.id].isCompleted = false;
        });
      }
      break;
    case CLEAR_COMPLETED:
      arrList.forEach(item => {
        if (item.isCompleted) {
          delete draft.list[item.id];
        }
      });
      break;
    case RESPON:
      draft.image = action.image;
      break;
  }
}, initialState);
export default homeReducer;
