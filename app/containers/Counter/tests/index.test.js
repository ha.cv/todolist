import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Counter from '../index';

describe('<Counter />', () => {
  it('defined', () => {
    const { container } = render(<Counter />);
    expect(container).toBeDefined();
  });

  it('event onclick', () => {
    const { getByTestId, getByText } = render(<Counter />);
    fireEvent.click(getByText('+'));
    expect(getByTestId('count')).toHaveTextContent('1');
  });
});
