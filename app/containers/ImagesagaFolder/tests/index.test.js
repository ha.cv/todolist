import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router-dom';
import * as appActions from 'containers/App/actions';
import { callAPI } from 'containers/HomePage/actions';
import Imagesaga from '../index';
import configureStore from '../../../configureStore';

const renderHomePage = store =>
  render(
    <Provider store={store}>
      <Imagesaga />
    </Provider>,
  );
describe('<Imagesaga />', () => {
  let store;
  beforeAll(() => {
    appActions.loadRepos = jest.fn(() => ({ type: '' }));
  });

  beforeEach(() => {
    store = configureStore({}, browserHistory);
    appActions.loadRepos.mockClear();
  });
  it('should fetch repos if the form is submitted when the click button', () => {
    const { container } = renderHomePage(store);
    store.dispatch(callAPI());
    const button = container.querySelector('button');
    fireEvent.click(button);
  });
});
